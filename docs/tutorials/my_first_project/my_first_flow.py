from __future__ import print_function

import os

from kabaret import flow


class EditAction(flow.Action):

    _file = flow.Parent()

    def get_buttons(self):
        self.message.set('<h2>Select an Editor</h2>')
        return ['Maya', 'Sublime']

    def run(self, button):
        # Here we would select an executable depending on the button
        # or the file extension or anything really...
        # and use subprocess to run the editor.
        print('Editing the file:', self._file.filename.get())


class File(flow.Object):

    task = flow.Parent()
    filename = flow.Computed()
    edit = flow.Child(EditAction)

    def get_filename(self):
        project = self.root().project()
        store = project.settings.store.get()
        task_name = self.task.name()
        name = self.name()
        ext = '.ma'
        return os.path.join(store, project.name(), task_name, name)+ext

    def compute_child_value(self, child_value):
        if child_value is self.filename:
            self.filename.set(self.get_filename())


class TaskStatus(flow.values.ChoiceValue):

    CHOICES = ['INV', 'WIP', 'RTK', 'Done']


class Task(flow.Object):

    shot = flow.Parent()
    status = flow.Param('INV', TaskStatus).watched()
    scene = flow.Child(File)

    def child_value_changed(self, child_value):
        if child_value is self.status:
            self.send_mail_notification()
            self.shot.update_status()

    def send_mail_notification(self):
        # If this was a real task there would be an assignee
        # that we could send a mail to...
        print(
            'Mailing to santa: status of Task {!r} is now {!r}'.format(
                self.oid(), self.status.get()
            )
        )

class Shot(flow.Object):

    first = flow.IntParam(1).watched()
    last = flow.IntParam(100).watched()
    length = flow.Computed()

    anim = flow.Child(Task)
    lighting = flow.Child(Task)
    comp = flow.Child(Task)

    status = flow.Param('NYS').ui(editable=False)

    def update_status(self):
        status = 'WIP'
        statuses = set([
            task.status.get()
            for task in (self.anim, self.lighting, self.comp)
        ])
        if len(statuses) == 1:
            status = statuses.pop()

        self.status.set(status)

    def child_value_changed(self, child_value):
        '''
        Called when a watched child Value has changed.
        '''
        if child_value in (self.first, self.last):
            # We invalidate self.length whenever self.first or self.last 
            # changes:
            self.length.touch()

    def compute_child_value(self, child_value):
        '''
        Called when a ComputedValue needs to deliver its result.
        '''
        if child_value is self.length:
            self.length.set(
                self.last.get()-self.first.get()+1
            )

class AddShotAction(flow.Action):

    _shots = flow.Parent()

    shot_name = flow.Param('shot000')
    first_frame = flow.IntParam(1)
    last_frame = flow.IntParam(100)

    def get_buttons(self):
        return ['Create Shot', 'Cancel']

    def run(self, button):
        if button == 'Cancel':
            return

        # Real life scenario should validate this value:
        shot_name = self.shot_name.get().strip()

        # Create the shot using our Parent() relation:
        shot = self._shots.add(shot_name)

        # Configure the shot with requested values:
        shot.first.set(self.first_frame.get())
        shot.last.set(self.last_frame.get())

        # Tell everyone that the Shots list has changed
        # and should be reloaded:
        self._shots.touch()


class Shots(flow.Map):

    add_shot = flow.Child(AddShotAction)

    @classmethod
    def mapped_type(cls):
        return Shot

    def columns(self):
        return ['Name', 'Ranges']

    def _fill_row_cells(self, row, item):
        row['Name'] = item.name()
        row['Ranges'] = '{}->{}'.format(item.first.get(), item.last.get())

    def _fill_row_style(self, style, item, row):
        style['icon'] = ('icons.status', item.status.get())

class ProjectSettings(flow.Object):

    store = flow.Param('/tmp/PROJECTS')
    framerate = flow.IntParam(24)
    image_height = flow.IntParam(1080)
    image_width = flow.IntParam(1920)


class Project(flow.Object):

    shots = flow.Child(Shots)
    settings = flow.Child(ProjectSettings)

