===
GUI
===

.. warning:: Documentation in Progress...

Overview
========
	

View Controls
=============

Showing and Hiding Views
------------------------

Duplicate View
--------------

Maximizing View
---------------

Stacking Views
--------------


Flow View
=========

Project Navigation
------------------

Hidden Shortcuts
----------------

Ctrl+Click
^^^^^^^^^^

Middle Mouse Button
^^^^^^^^^^^^^^^^^^^
