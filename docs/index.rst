.. Kabaret documentation master file, created by
   sphinx-quickstart on Fri Jan  5 17:58:31 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Kabaret's documentation
==================================

**Kabaret** is a Free and Open Source VFX/Animation Studio Framework.

It is made for TDs and Scripters involved in Production Tracking, Asset Management, Workflow and Pipelines used by Production Managers and CG Artists.

Main Features
-------------

* Fast and Easy Project modeling.
* No decision made for you, your pipe = your rules !
* Based on 20+ years of `experience <credits.html>`_ in the field.
* Generative end-user GUI: zero code *needed*.
* Modular and Extendable pure-python architecture.
* Plugin system for zero/low code modularity.
* Python 2.7+ and 3.6+ compatible.
* Embeddable in PyQt4, PySide, PyQt5, PySide2 and Blender applications.
* Tested under Windows and Linux.
* There's an example project so you can start experimenting in less than 5 minutes !

Status
------
**Kabaret** has been used in productions for over 7 years for Commercials, Teasers, TV Shows and Feature Movies.

Among the 20+ releases made since its source opening, only one single (and minor) breaking change was introduced.

.. toctree::
   :caption: Introduction
   :maxdepth: 2

   why_and_how.rst

.. .. toctree::
..    :caption: Quick Tour
..    :maxdepth: 2

..    gui_quick_tour.rst
..    flow_quick_tour.rst
..    extending_quick_tour.rst
..    featured_extensions.rst

.. toctree::
   :caption: Tutorials
   :maxdepth: 2

   tutorials/tutorials.rst

.. toctree::
   :caption: Documentations
   :maxdepth: 2

   install.rst
   user_guide.rst
   flow_reference_guide.rst
   app_reference_guide.rst
   featured_extensions.rst
   plugin_system.rst

.. toctree::
   :caption: More
   :maxdepth: 2
   
   faq.rst
   credits.rst
   
Indices
=======
* :ref:`genindex`
* :ref:`modindex`
