===================
App Reference Guide
===================

.. note:: This documentation in still in progress. Your feedback is welcome :)

The **kabaret** framework is a modular system that will help you create
standalone or embedded applications.

When building a application, you will start by creating a
:py:class:`kabaret.app.session.KabaretSession` (or more likely, one
of its subclasses).

A **Session** contains some **Actors** that each provide specific
functionnalities and **commands** to use them.

A **Session** containts some **Views** that use the **commands** to
display and modify the informations managed by **Actors**.

A **Session** manages the **communication** between other sessions on the
network, deals with **UI** (events, layouts, ...) and **logging**.

Frameword devellopers can extend **kabaret** by providing new **Actors**
and/or **Views**.
When a User wants to use an extension, he must subclass a **Session** to
register the additionnal **Actors** / **Views**.

Once you session is properly configured, you can use one or more
instances of it in a single python interpretor. But you can't share sessions
between threads.

You can define multiple session types: a standalone GUI for User, another
for administration, a headless one acting as a worker waiting for orders, ...

You can navigate to the `Create My Studio <./tutorials/create_my_studio.html>`_ tutorial to get you started.


Session
=======

.. automodule:: kabaret.app.session
    :members:

Resources
=========
.. automodule:: kabaret.app.resources
	:members:
